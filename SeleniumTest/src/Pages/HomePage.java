package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;

public class HomePage {

	WebDriver driver;

	By homePageOrders = By.linkText("FIND YOUR ORDERS");
	By homePageSpecialOffers = By.linkText("SPECIAL OFFERS");
	By homePageContactUs = By.linkText("CONTACT US");
	By orderId = By.linkText("1111121#1111121");
	By splOffrs = By.linkText("TRACK IT NOW >>");
	By logout = By.linkText("Logout");
	By screen = By.xpath("//li[@data-slide-to='0' and @data-target='#myCarousel']");
	boolean isChecked = true;

	public HomePage(WebDriver driver){
		this.driver = driver;
	}

	//Get the User name from Home Page
	public String validateHomePage(){
		return driver.findElement(homePageOrders).getText()+" "+driver.findElement(homePageSpecialOffers).getText()+"  "+
				driver.findElement(homePageContactUs).getText();
	}

	public void selectOrder() throws Exception {
		Thread.sleep(2000);
		driver.findElement(homePageOrders).click();
		Thread.sleep(2000);
		while(isChecked){
			if(!driver.findElement(orderId).isDisplayed())
				driver.findElement(homePageOrders).click();
			if(driver.findElement(orderId).isDisplayed()){
				isChecked = false;
				break;
			}
		}
		driver.findElement(orderId).click();
		Thread.sleep(2000);
	}

	public void splOffers() throws Exception{
		Thread.sleep(2000);
		driver.findElement(screen).click();
		if(driver.findElement(splOffrs).isDisplayed()){
			Assert.fail();
		}
	}

	public void logout(){
		driver.findElement(logout).click();
	}

}
