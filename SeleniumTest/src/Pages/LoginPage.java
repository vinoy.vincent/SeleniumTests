package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class LoginPage {

	WebDriver driver;

	By userName = By.id("username");
	By password = By.id("password");
	By titleText =By.className("barone");
	By login = By.cssSelector(".btn.btn-success.btn-block");

	public LoginPage(WebDriver driver){
		this.driver = driver;

	}

	//Set user name in text box
	public void setLoginForm(){
		driver.findElement(login).click();

	}

	//Set user name in text box
	public void setUserName(String strUserName){
		driver.findElement(userName).clear();
		driver.findElement(userName).sendKeys(strUserName);;
	}

	//Set password in password text box
	public void setPassword(String strPassword){
		driver.findElement(password).clear();
		driver.findElement(password).sendKeys(strPassword);
	}

	//Click on login button
	public void clickLogin(){
		driver.findElement(login).click();
	}

	//Get the title of Login Page
	public String getLoginTitle(){
		return    driver.findElement(titleText).getText();
	}

	/**
	 * This POM method will be exposed in test case to login in the application
	 * @param strUserName
	 * @param strPasword
	 * @return
	 */

	public void login(String strUserName,String strPasword){

		//this.setLoginForm();
		//Fill user name
		this.setUserName(strUserName);

		//Fill password
		this.setPassword(strPasword);

		//Click Login button
		this.clickLogin();        
	}
}
