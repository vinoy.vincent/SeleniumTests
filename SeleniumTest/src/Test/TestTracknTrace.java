package Test;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import Pages.HomePage;
import Pages.LoginPage;

public class TestTracknTrace {

	static { System.setProperty("webdriver.chrome.driver", "D:\\Drivers\\chromedriver.exe");}

	WebDriver driver;
	LoginPage objLogin;
	HomePage objHomePage;

	@BeforeMethod
	public void setup(){
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		//driver.get("http://demo.guru99.com/V4/");
		driver.get("http://10.155.224.54:3000");

	}

	/**
	 *	1. Login to application
	 *  Log in to Track & Trace, check whether the 3 main menus are getting displayed and logout.
	 */

	@Test(priority=0, enabled=false)
	public void validateTracknTraceHome(){

		try{
			//Create Login Page object
			objLogin = new LoginPage(driver);

			//login to application
			objLogin.login("i@i.com", "infy123");

			// go the next page
			objHomePage = new HomePage(driver);
			Thread.sleep(2000);

			//Verify home page
			Assert.assertTrue(objHomePage.validateHomePage().toLowerCase().contains("find your orders special offers  contact us"));
			
			objHomePage.logout();

		}catch(Exception e){
			System.out.println("Exception :"+e.toString());
			Assert.fail();
		}finally
		{
			driver.quit();
		}
	}
	
	
	/**
	 *  2.Login to application
	 *  Log in to Track & Trace, Select an order from 'FIND YOUR ORDERS' drop down, then log out.
	 */
	
	@Test(priority=1, enabled=false)
	public void validateTracknTrace(){
		
		try{
			//Create Login Page object
			objLogin = new LoginPage(driver);
			objLogin.login("i@i.com", "infy123");
			// go the next page
			objHomePage = new HomePage(driver);
			objHomePage.selectOrder();
			objHomePage.logout();
			
		}catch(Exception e){
			System.out.println("Exception :"+e.toString());
			Assert.fail();
		}finally
		{
			driver.quit();
		}
	}
	
	/**
	 *  3.Login to application
	 *  Log in to Track & Trace, check whether the special offers page is getting displayed.
	 */
	
	@Test(priority=2, enabled=true)
	public void validateTracknTraceSplOffers(){
		
		try{
			objLogin = new LoginPage(driver);
			objLogin.login("i@i.com", "infy123");
			objHomePage = new HomePage(driver);
			objHomePage.splOffers();
			objHomePage.logout();
			
		}catch(Exception e){
			System.out.println("Exception :"+e.toString());
			Assert.fail();
		}finally
		{
			driver.quit();
		}
	}
}



